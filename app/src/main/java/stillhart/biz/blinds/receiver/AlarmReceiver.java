package stillhart.biz.blinds.receiver;

        import android.app.AlarmManager;
        import android.app.PendingIntent;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.widget.Toast;
        import stillhart.biz.blinds.ui.MainActivity;

        import java.util.Calendar;

/**
 * Created by About  23 Ninjas on 25.03.2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context ctx, Intent intent) {
        Toast.makeText(ctx, "Getting Blinds up!", Toast.LENGTH_LONG).show();

        try {
            MainActivity.call("/up");
            Thread.sleep(1500);
            MainActivity.call("/up");
            Thread.sleep(1500);
            MainActivity.call("/up");
            Thread.sleep(1500);
            MainActivity.call("/up/full");

        }catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(intent.getPackage());
    }

    public static final String TIME_SAVE = "TimeSave";

    public static void setAlarm(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(TIME_SAVE, 0);
        System.out.println(prefs.getInt("hour", 0) + " b " + prefs.getInt("minute", 0));
        if(prefs.getInt("hour", 0) != 0) {
            setInternAlarm(ctx, prefs.getInt("hour", 0), prefs.getInt("minute", 0));
        }

    }

    public static void setAlarm(Context ctx, int hour, int minute) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences(TIME_SAVE, 0).edit();
        editor.putInt("hour", hour);
        editor.putInt("minute", minute);
        editor.apply();

        Toast.makeText(ctx, "Setting Alarm: " + niceTime(hour, minute), Toast.LENGTH_LONG).show();

        setInternAlarm(ctx, hour, minute);
    }

    public static void removeAlarm(Context ctx) {
        AlarmManager alarmMgr = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ctx, AlarmReceiver.class);
        PendingIntent p = PendingIntent.getBroadcast(ctx, 0, intent, 0);
        alarmMgr.cancel(p);
        p.cancel();
    }

    private static void setInternAlarm(Context ctx, int hour, int minute) {

        removeAlarm(ctx);

        AlarmManager alarmMgr = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ctx, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);

        if(calendar.before(Calendar.getInstance())){
            calendar.add(Calendar.DATE, 1);
        }

        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
    }

    private static String niceTime(int hour, int minute) {
        return String.format("%02d", hour) +  ":" + String.format("%02d", minute);
    }
}
