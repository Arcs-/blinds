package stillhart.biz.blinds.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by About  23 Ninjas on 25.03.2016.
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            System.out.println("set alarm from startup");
            AlarmReceiver.setAlarm(context);
        }
    }

}