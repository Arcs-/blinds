package stillhart.biz.blinds.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TimePicker;
import org.json.JSONException;
import org.json.JSONObject;
import stillhart.biz.blinds.R;
import stillhart.biz.blinds.helper.JsonTools;
import stillhart.biz.blinds.receiver.AlarmReceiver;

import java.io.IOException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Context self = this;

        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call("/hello");
            }
        });

        final TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        SharedPreferences prefs = getSharedPreferences(AlarmReceiver.TIME_SAVE, 0);
        if(prefs.getInt("hour", 0) != 0) {
            timePicker.setCurrentHour(prefs.getInt("hour", 0));
            timePicker.setCurrentMinute(prefs.getInt("minute", 0));
        }

        Button btnSet = (Button) findViewById(R.id.btnSet);
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePicker.clearFocus();
                AlarmReceiver.setAlarm(self, timePicker.getCurrentHour(), timePicker.getCurrentMinute());
            }
        });
        Button btnRemove = (Button) findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmReceiver.removeAlarm(self);
            }
        });


        ImageButton btnUP = (ImageButton) findViewById(R.id.btnUP);
        btnUP.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                call("/up");
            }
        });
        btnUP.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                call("/up/full");
                return true;
            }
        });

        ImageButton btnDown = (ImageButton) findViewById(R.id.btnDOWN);
        btnDown.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                call("/down");
            }
        });
        btnDown.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                call("/down/full");
                return true;
            }
        });


    }

    public static void call(final String params) {

        new Thread(new Runnable(){
            @Override
            public void run() {

                try {
                    JSONObject answer = JsonTools.readObjectFromUrl(new URL("http://192.168.1.103:8888" + params));
                    if(answer.getString("status").equals("okay")) System.out.println("DO! " + params);
                    else System.out.println(answer);

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

            }
        }).start();


    }

}
